# Vorlagenvariablen
## ZUORDNEN
```
{{$wisdom := .}}

```
## BENUTZEN
```
{{$wisdom}}
```
Eine Pipeline in einer Aktion kann eine Variable initialisieren, um das Ergebnis zu erfassen. Die Initialisierung hat Syntax
```
Wobei $ Variable der Name der Variablen ist. Eine Aktion, die eine Variable deklariert, erzeugt keine Ausgabe.
```
Wenn eine Aktion "Bereich" eine Variable initialisiert, wird die Variable auf die nachfolgenden Elemente der Iteration gesetzt. Ein "Bereich" kann auch zwei Variablen angeben, die durch ein Komma getrennt sind:
In diesem Fall werden $ index und $ element auf die sukzessiven Werte des Array / Slice-Indexes bzw. Map-Key und -Elements gesetzt. Beachten Sie, dass wenn nur eine Variable vorhanden ist, das Element zugewiesen wird. dies ist entgegengesetzt zur Konvention in Go-Range-Klauseln.

Der Gültigkeitsbereich einer Variablen erstreckt sich auf die Aktion "Ende" der Kontrollstruktur ("if", "with" oder "range"), in der sie deklariert ist, oder auf das Ende der Vorlage, wenn keine solche Kontrollstruktur vorhanden ist. Ein Vorlagenaufruf erbt Variablen nicht von dem Punkt seines Aufrufs.

Wenn die Ausführung beginnt, wird $ auf das Datenargument gesetzt, das an Execute übergeben wird, dh an den Startwert von dot.
