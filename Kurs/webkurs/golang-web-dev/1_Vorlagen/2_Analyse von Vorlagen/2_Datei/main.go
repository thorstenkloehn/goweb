package main

import (
	"os"
	"text/template"
	"log"
)

func main() {

	Vorlagen, fehler := template.ParseFiles("Test.html")
	if fehler != nil {
		log.Fatal("Die Vorlage bearbeiten",fehler)
	}
	Datei,fehler := os.Create("index.htm")
	if fehler != nil {
		log.Fatal ("Datei kann nicht geschrieben werden")
	}
	defer Datei.Close()
	fehler = Vorlagen.Execute(Datei,nil)
	if fehler != nil {
			log.Fatal ("Datei kann nicht geschrieben werden")
	}
}
