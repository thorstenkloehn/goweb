package main

import (
	"os"
	"log"
	"text/template"
)

func main() {

	vorlagenzeiger, fehler := template.ParseGlob("Vorlagen/*")

	if fehler != nil {

		log.Fatal("Fehler",fehler)
	}

	fehler = vorlagenzeiger.ExecuteTemplate(os.Stdout , "Test.html",nil)
	
	if fehler != nil {
		log.Fatal("Fehler",fehler)
	}

	fehler = vorlagenzeiger.ExecuteTemplate(os.Stdout, "Hallo.html",nil )

	if fehler != nil {
		log.Fatal("Fehler",fehler)
	}
}