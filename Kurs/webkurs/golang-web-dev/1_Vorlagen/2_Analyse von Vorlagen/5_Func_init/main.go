package main

import (
	"os"
	"text/template"
	"log"
)

var Vorlagezeiger *template.Template

func init() {

	Vorlagezeiger = template.Must(template.ParseGlob("Vorlage/*"))
}
func main() {

	fehler := Vorlagezeiger.Execute(os.Stdout,nil)

	if fehler != nil {
		log.Fatal("Test",fehler)
	}
	fehler = Vorlagezeiger.ExecuteTemplate(os.Stdout,"Test.html",nil)
}
