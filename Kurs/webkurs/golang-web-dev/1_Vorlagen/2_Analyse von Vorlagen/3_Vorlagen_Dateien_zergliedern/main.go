package main

import (
	"log"
	"os"
	"text/template"
)

func main() {

	vorlagezeiger, fehler := template.ParseFiles("DateiName.test", "start.html")
	if fehler != nil {
		log.Fatal("Fehler Datei", fehler)
	}
	fehler = vorlagezeiger.Execute(os.Stdout, nil)
	if fehler != nil {

		log.Fatal("Test", fehler)
	}

	fehler = vorlagezeiger.ExecuteTemplate(os.Stdout, "start.html", nil)
	if fehler != nil {
		log.Fatal("Test", fehler)
	}

}
