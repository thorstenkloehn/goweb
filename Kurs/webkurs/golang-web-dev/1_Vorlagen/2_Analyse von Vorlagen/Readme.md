
## Vorlage eingeben

### template.Template Quellcode
var Vorlagen * template.Template neue Template Zeiger erstellen.


## Vorlagen Syntaxanalyse

### template.ParseFiles
ParseFiles erstellt eine neue Vorlage und analysiert die Vorlagendefinitionen aus den benannten Dateien. Der Name der zurückgegebenen Vorlage hat den Basisnamen und den analysierten Inhalt der ersten Datei. Es muss mindestens eine Datei geben. Wenn ein Fehler auftritt, stoppt die Analyse und die zurückgegebene * Vorlage ist null.


### template.ParseGlob
ParseGlob erstellt eine neue Vorlage und analysiert die Vorlagendefinitionen anhand der durch das Muster identifizierten Dateien, die mindestens einer Datei entsprechen müssen. Die zurückgegebene Vorlage enthält den (Basis-) Namen und den (analysierten) Inhalt der ersten Datei, die dem Muster entsprechen. ParseGlob entspricht dem Aufruf von ParseFiles mit der Liste der Dateien, die dem Muster entsprechen.

### template.Parse

Parse analysiert Text als Vorlagenkörper für t. Benannte Template-Definitionen ({{define ...}} oder {{block ...}} Anweisungen) im Text definieren zusätzliche Vorlagen, die mit t assoziiert sind und aus der Definition von t selbst entfernt werden.

## Vorlagen ausführen

### template.Execute
Execute wendet eine analysierte Vorlage auf das angegebene Datenobjekt an und schreibt die Ausgabe in wr. Wenn beim Ausführen der Vorlage oder beim Schreiben der Ausgabe ein Fehler auftritt, wird die Ausführung gestoppt, es können jedoch bereits Teilergebnisse in den Ausgabe-Writer geschrieben worden sein. Eine Vorlage kann sicher parallel ausgeführt werden. Wenn parallele Ausführungen jedoch einen Writer gemeinsam nutzen, kann die Ausgabe interleaved sein.
### template.ExecuteTemplate
ExecuteTemplate wendet die mit t verknüpfte Vorlage mit dem angegebenen Namen auf das angegebene Datenobjekt an und schreibt die Ausgabe in wr. Wenn beim Ausführen der Vorlage oder beim Schreiben der Ausgabe ein Fehler auftritt, wird die Ausführung gestoppt, es können jedoch bereits Teilergebnisse in den Ausgabe-Writer geschrieben worden sein. Eine Vorlage kann sicher parallel ausgeführt werden. Wenn parallele Ausführungen jedoch einen Writer gemeinsam nutzen, kann die Ausgabe interleaved sein.
## Hilfreiche Vorlagenfunktionen

### template.Must
Must ist ein Hilfsprogramm, das einen Aufruf an eine Funktion zurückgibt (* Vorlage, Fehler) und in Panik versetzt wird, wenn der Fehler nicht null ist. Es ist für die Verwendung in Variableninitialisierungen wie z
### template.New
Neu weist eine neue undefinierte Vorlage mit dem angegebenen Namen zu.
### os.Stdout
Der Standard-Ausgabestream ist das Standardziel für die Ausgabe von Anwendungen

