# Übergeben von Daten an Vorlagen
## Sie bekommen einen Wert - das ist es!

Glücklicherweise haben wir viele verschiedene Typen, bei denen dieser Wert zusammengesetzte Typen enthalten kann, die zusammen Werte bilden. (Diese werden auch als Aggregatdatentypen bezeichnet - sie aggregieren viele verschiedene Werte).

### Slice
Verwenden Sie dies, um eine Reihe von Werten desselben Typs zu übergeben. Wir könnten eine [] int oder eine [] Zeichenkette oder eine Scheibe eines beliebigen Typs haben
### Map
Verwenden Sie dies zum Übergeben von Schlüsselwertdaten.
### Struct
Dies ist wahrscheinlich der am häufigsten verwendete Datentyp beim Übergeben von Daten an Vorlagen. Eine Struktur ermöglicht das Zusammensetzen von Werten verschiedener Typen.