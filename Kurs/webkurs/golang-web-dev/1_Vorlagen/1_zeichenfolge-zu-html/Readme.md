## os.Create

Dies ermöglicht uns, eine Datei zu erstellen.

## defer

Mit dem Schlüsselwort defer können wir die Ausführung einer Anweisung verzögern, bis die Funktion, in die wir die Defer-Anweisung gestellt haben, zurückgegeben wird.

## io.Copy

Dies ermöglicht uns, von einer Quelle zu einem Ziel zu kopieren.

## strings.NewReader

NewReader gibt einen neuen Reader zurück, der von s liest.

## os.Args

Args ist eine Variable aus dem Paket os. Args enthalten die Befehlszeilenargumente, beginnend mit dem Programmnamen.

## fmt.Sprintf()

formatiert die Standardformate für seine Operanden und gibt die resultierende Zeichenfolge zurück.
Leerzeichen werden zwischen Operanden hinzugefügt, wenn keines eine Zeichenfolge ist.

## log.Fatal
Fatal entspricht Print () gefolgt von einem Aufruf von os.Exit (1).

## 